import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
// This was Rodney Hayes' code that he emailed as an example
public class AddressBook {
	
	// New hash map to store address entries
	Map<String, AddressEntry> addressBook = new HashMap<String, AddressEntry>();
	
	// Method for a new entry into the addressBook map
	public void newEntry(String firstName, String lastName, String street, String city, String state, String zip, String telephone, String email){
		
		// Create new entry using AddressEntry constructor
		AddressEntry personInfo = new AddressEntry(firstName, lastName, street, city, state, zip, telephone, email);
		
		// Add entry to map using the email as the key
		String theKey = email;
		theKey.toLowerCase();
		addressBook.put(theKey, personInfo);
	}
	
	// Lists all of the entries in the map
	public void list(){
		
		// Loops through each key and accesses the information associated with each key
		for(String key : addressBook.keySet()){
			AddressEntry returnInfo = addressBook.get(key);
			System.out.print(returnInfo.toString());
		}
	}

}
